#!/usr/bin/python

import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(("0.0.0.0", 8081))
s.listen(2)

while True:
    c = s.accept()[0]
    print c.recv(10000)
    #print c.recv(10000)
    d = open("example.html").read()
    c.send("HTTP/1.1 200 OK\r\nContent-Length: %d\r\n\r\n" % len(d))
    c.send(d)
    c.close()
