/*
 * Copyright 2013 Nikita Schmidt
 *
 * This file is part of VFSCheck.
 *
 * VFSCheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VFSCheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VFSCheck.  If not, see <http://www.gnu.org/licenses/>.
 */

package vfs.check;

import android.app.Activity;
import android.content.*;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import android.graphics.Color;
import android.text.format.DateUtils;
import android.text.TextUtils;
import java.util.Formatter;
import java.util.Calendar;

public final class ViewActivity extends Activity
{
    public static Updater updater = new Updater();

    // Called when the activity is first created.
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        VisaStati v = Storage.read(this);

        if (v.isEmpty()) {
            startActivity(new Intent(this, ConfigActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
            return;
        }

        if (v.size() > 1) {
            startActivity(new Intent(this, MultiViewActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
            return;
        }

        VisaStatus vs = v.firstEntry().getValue();

        updater.set_adapter(new VisaStatusAdapter(this, vs));

        setContentView(R.layout.view);

        ((ListView) findViewById(R.id.list)).setAdapter(updater.adapter);

        updater.draw_new(vs);
    }

    public void onDestroy()
    {
        super.onDestroy();
        updater.set_adapter(null);
    }

    // Activity goes into foreground.
    @Override
    protected void onResume()
    {
        super.onResume();

        // ConfigActivity may have changed the details we are tracking
        VisaStati v = Storage.read(this);

        if (v.size() > 1)
            startActivity(new Intent(this, MultiViewActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

        if (v.size() != 1) {
            finish();
            return;
        }

        VisaStatus vs = v.firstEntry().getValue();
        if (!vs.same_as(updater.adapter.v))
            updater.draw_new(vs);

        // tell Notifier we don't want status bar notifications and
        // cancel any that may have been issued already
        Notifier.stop_status_bar(this);
    }

    // Activity goes into background.
    @Override
    protected void onPause()
    {
        super.onPause();
        // tell Notifier to use the status bar for notifications
        Notifier.start_status_bar();
    }

    // Populate the action bar and overflow menu.
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        updater.menu_item = menu.findItem(R.id.menu_update);
        return true;
    }

    // Called when a menu item is selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.menu_update:
                Intent intent = new Intent("vfs.check.REFRESH", null, this,
                                           VFSBroadcastReceiver.class);
                sendBroadcast(intent);
                return true;

            case R.id.menu_config:
                startActivity(new Intent(this, ConfigActivity.class));
                return true;

            case R.id.menu_about:
                startActivity(new Intent(this, AboutActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static final class Updater implements Runnable
    {
        private boolean appdate_shown = false;
        private volatile VisaStatus visa_status;
        public VisaStatusAdapter adapter;
        public MenuItem menu_item;

        // Update associated ViewActivity with new data.
        // Pass null to indicate that a refresh is in progress.
        // This may be called on any thread.
        public synchronized void show_on_ui_thread(int idx, VisaStatus v)
        {
            if (idx != 0) {
                visa_status = v;
                if (adapter != null)
                    adapter.activity.runOnUiThread(this);
            }
        }

        // Set or remove an adapter.
        public synchronized void set_adapter(VisaStatusAdapter a)
        {
            adapter = a;
            appdate_shown = false;
        }

        // Draw v from scratch.
        public void draw_new(VisaStatus v)
        {
            ((TextView) adapter.activity.findViewById(R.id.reference))
                .setText(adapter.activity.getString(v.vp.country) + ", " + v.reference);

            visa_status = v;
            appdate_shown = false;
            run();
        }

        public void run()
        {
            if (adapter == null)
                return;         // activity destroyed, nothing to do here

            VisaStatus v = visa_status; // cache volatile member

            if (v == null) {
                // refresh in progress
                if (menu_item != null)
                    menu_item.setActionView(R.layout.busy);
                return;
            }

            adapter.v = v;

            Calendar c = v.appdate;
            if (!appdate_shown && c != null) {
                StringBuilder sb = new StringBuilder();
                Formatter fmt = new Formatter(sb);
                fmt.format(adapter.activity.getString(R.string.appdate), c, c, c);
                ((TextView) adapter.activity.findViewById(R.id.appdate)).setText(sb);
                appdate_shown = true;
            }

            ((TextView) adapter.activity.findViewById(R.id.error)).setText(
                v.error == 0 ? "" : adapter.activity.getString(v.error));

            ((TextView) adapter.activity.findViewById(R.id.lastupd)).setText(
                TextUtils.concat(
                    adapter.activity.getString(R.string.lastupd),  // "Updated"
                    " ",
                    DateUtils.getRelativeTimeSpanString(adapter.activity,
                            v.updated.getTime(), true)
                )
            );

            if (menu_item != null)
                menu_item.setActionView(null);

            adapter.notifyDataSetChanged();
        }
    }
}

final class VisaStatusAdapter extends BaseAdapter
{
    public final Activity activity;
    public VisaStatus v;

    public VisaStatusAdapter(Activity a, VisaStatus vs)
    {
        super();
        activity = a;
        v = vs;
    }

    public int   getCount()             { return v.entries.size(); }
    public Object getItem(int position) { return v.entries.get(v.entries.size() - position - 1); }
    public long getItemId(int position) { return position; }

    public View getView(int position, View cv, ViewGroup parent)
    {
        if (cv == null)
            cv = new EntryView(activity);
        int rpos = v.entries.size() - position - 1;
        ((EntryView) cv).fill(v.entries.get(rpos), position == 0 ? Color.CYAN : Color.LTGRAY);
        return cv;
    }

    private static class EntryView extends LinearLayout
    {
        private TextView status_view;
        private TextView date_view;

        public EntryView(Context context)
        {
            super(context);
            setOrientation(VERTICAL);

            status_view = new TextView(context);
            date_view = new TextView(context);

            date_view.setTextSize(11);
            date_view.setTextColor(Color.YELLOW);

            addView(status_view, new LayoutParams(LayoutParams.MATCH_PARENT,
                        LayoutParams.WRAP_CONTENT));
            LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT);
            lp.gravity = Gravity.RIGHT;
            addView(date_view, lp);
        }

        public void fill(VisaStatus.Entry e, int colour)
        {
            StringBuilder sb = new StringBuilder();
            Formatter fmt = new Formatter(sb);
            fmt.format("%ta, %te %tB", e.status_date, e.status_date, e.status_date);
            date_view.setText(sb);
            status_view.setText(e.status);
            status_view.setTextColor(colour);
        }
    }
}
