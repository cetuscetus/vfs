/*
 * Copyright 2013 Nikita Schmidt
 *
 * This file is part of VFSCheck.
 *
 * VFSCheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VFSCheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VFSCheck.  If not, see <http://www.gnu.org/licenses/>.
 */

package vfs.check;

import java.net.*;
import java.io.*;
import java.util.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.dom.DOMResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
//import android.util.Log;

public final class VisaStatus
{
    public static final class Entry
    {
        public String   raw_status;
        public Date     timestamp;
        public Calendar status_date;
        public String   status;

        public Entry(String app_status, Date tstamp, VisaStatus v)
        {
            raw_status = app_status;
            timestamp = tstamp;

            int space = app_status.lastIndexOf(' ');
            int full_stop = app_status.length();
            if (app_status.charAt(full_stop - 1) == '.')
                --full_stop;

            if (space > 3)
                try {
                    status_date = v.parse_date_from_server(
                            app_status.substring(space + 1, full_stop));
                    if (app_status.substring(space - 3, space).equalsIgnoreCase(" on"))
                        space -= 3;
                    status = app_status.substring(0, space).trim();
                    return;
                } catch (Exception exc) {
                }

            status_date = new GregorianCalendar();
            status_date.setTime(tstamp);
            status = app_status;
        }
    }

    public VisaProvider vp;             // VFS server details
    public int          vp_idx;         // vp's index in 'providers' array

    public  final String   reference;   // application reference string
    public  final Calendar birthday;    // applicant's date of birth or passport expiry date
    public  final String   lastname;    // applicant's last name (when required)
    private final String[] subref;      // parsed components of reference

    public Calendar    appdate;         // application submission date
    public Date        updated;         // time of last check with server
    public int         error;           // resource ID of error message
    public boolean     done;            // whether this no longer needs updating

    // status records retrieved from server
    public LinkedList<Entry> entries = new LinkedList<Entry>();

    public VisaStatus(String ref, String dob) throws FormatException
    {
        reference = ref;
        subref = ref.split("/");

        if (subref.length != 4 || subref[0].length() != 4 || subref[1].length() != 6)
            throw new FormatException(R.string.bad_ref);

        try {
            birthday = parse_date_from_ui(dob);
        } catch (Exception e) {
            throw new FormatException(R.string.bad_date);
        }

        done = false;
        lastname = null;        // for now
    }

    // compare registration details
    public boolean same_as(VisaStatus v)
    {
        // can't compare two Calendars for equality because they may
        // be in different time zones
        return v != null && reference.equals(v.reference)
            && birthday.get(Calendar.DATE)  == v.birthday.get(Calendar.DATE)
            && birthday.get(Calendar.MONTH) == v.birthday.get(Calendar.MONTH)
            && birthday.get(Calendar.YEAR)  == v.birthday.get(Calendar.YEAR);
    }

    // Load visa status from the provider specified in vp,
    // or try all providers in turn if vp is not set.
    // Return true if new entry appeared.
    // Throw VFSException if server's reply did not contain expected fields.
    // Throw Exception on many other occasions.
    public boolean load() throws Exception
    {
        if (vp == null) {
            Exception e = null;
            for (vp_idx = 2; vp_idx < VisaProvider.providers.length; vp_idx++)
                try {
                    vp = VisaProvider.providers[vp_idx];
                    //Log.i("vfs.check", reference.substring(0, 4) + ": " +
                    //        vp_idx + ": trying " + vp.url);
                    boolean r = load();
                    //Log.i("vfs.check", reference.substring(0, 4) + ": " +
                    //        vp_idx + ": found");
                    return r;
                } catch (VFSException exc) {
                    //Log.i("vfs.check", reference.substring(0, 4) + ": " +
                    //        vp_idx + ": not found", exc);
                } catch (Exception exc) {
                    //Log.i("vfs.check", reference.substring(0, 4) + ": " +
                    //        vp_idx + ": exception", exc);
                    e = exc;
                }
            throw e == null ? new VFSException() : e;
        }

        URL url = new URL(vp.url);
        //url = new URL("http", "192.168.2.100", 8081, url.getFile());
        HttpURLConnection c = (HttpURLConnection) url.openConnection();

        try {
            c.setConnectTimeout(20000);
            c.setReadTimeout(15000);
            c.setDoOutput(true);
            c.setRequestProperty("Connection", "close");

            Formatter out = new Formatter(c.getOutputStream(), "utf-8", Locale.UK);
            out.format(vp.post,
                    subref[0], subref[1], subref[2], subref[3],
                    birthday, birthday, birthday);
            out.flush();

            BufferedInputStream in = new BufferedInputStream(c.getInputStream());
            //BufferedInputStream in = new BufferedInputStream(new org.apache.commons.io.input.TeeInputStream(c.getInputStream(), System.out));
            //VFSHandler handler = new VFSHandler();
            org.xml.sax.XMLReader parser = org.xml.sax.helpers.XMLReaderFactory.
                    createXMLReader("org.ccil.cowan.tagsoup.Parser");
            parser.setFeature("http://xml.org/sax/features/namespaces", false);
            //r.setContentHandler(handler);
            //r.parse(new org.xml.sax.InputSource(in));

            DOMResult dom = new DOMResult();
            TransformerFactory.newInstance().newTransformer().transform(
                    new SAXSource(parser, new org.xml.sax.InputSource(in)),
                    dom
            );

            out.close();
            in.close();

            XPath xpath = XPathFactory.newInstance().newXPath();
            String st = xpath.evaluate(vp.status_xpath, dom.getNode()).trim();

            if (st.isEmpty() || st.startsWith("Record does not exist"))
                throw new VFSException();

            if (appdate == null && vp.appdate_xpath != null)
                try {
                    appdate = parse_date_from_server(
                        xpath.evaluate(vp.appdate_xpath, dom.getNode()).trim()
                    );
                } catch (Exception exc) {
                    throw new VFSException();
                }

            updated = new Date();

            //Log.i("vfs.check", st);
            return process_app_status(st);
        } finally {
            c.disconnect();
        }
    }

    public static String date_to_string(Calendar date)
    {
        StringBuilder s = new StringBuilder();
        Formatter f = new Formatter(s, Locale.UK);
        f.format("%td/%tm/%tY", date, date, date);
        f.close();
        return s.toString();
    }

    private boolean process_app_status(String app_status)
    {
        if (!entries.isEmpty() &&
                entries.getLast().raw_status.equalsIgnoreCase(app_status)) {
            // if this is a new app version with an updated list of terminating
            // conditions, check whether a status from a previous version is
            // actually a final status
            check_if_done();
            return false;
        }

        Entry e = new Entry(app_status, new Date(), this);

        entries.addLast(e);
        check_if_done();

        return true;
    }

    public static Calendar parse_date_from_ui(String date) throws Exception
    {
        Scanner s = new Scanner(date);
        s.useDelimiter("[^0-9]+");
        int day = s.nextInt();
        int month = s.nextInt();
        int year = s.nextInt();
        if (s.hasNext())
            throw new IllegalArgumentException();
        Calendar c = new GregorianCalendar(year, month - 1, day);
        c.setLenient(false);
        c.getTime();            // validation
        return c;
    }

    public Calendar parse_date_from_server(String date) throws Exception
    {
        Scanner s = new Scanner(date);
        s.useLocale(Locale.UK);
        s.useDelimiter("/");
        int day, month, year;
        if (vp.mdy) {
            // strangely enough, the UK tracking site supplies dates in
            // American format
            month = s.nextInt();
            day = s.nextInt();
        } else {
            // normal date
            day = s.nextInt();
            month = s.nextInt();
        }
        year = s.nextInt();
        if (s.hasNext())
            throw new IllegalArgumentException();
        Calendar c = new GregorianCalendar(year, month - 1, day);
        c.setLenient(false);
        c.getTime();            // validation
        return c;
    }

    public void check_if_done()
    {
        if (!entries.isEmpty()) {
            String status = entries.getLast().status;
            String lcstat = status.toLowerCase(Locale.UK);
            if (lcstat.contains("application is collected")
             || lcstat.contains("application has been collected")
             || lcstat.contains("application has been picked up")
             || status.contains("выдано в Визовом центре")
             || status.contains("выдано в Визовом Центре")
             || lcstat.contains("application has been handed over to the applicant")
             || lcstat.contains("application collected")
             || lcstat.equals("case closed")
             || lcstat.contains("паспорт выдан в ")) {
                done = true;
            }
        }
    }


    public static void main(String[] args) throws Exception
    {
        VisaStatus v = new VisaStatus("SPAC/191112/116385/1", "03/01/1980");

        v.vp_idx = 0;
        v.vp = VisaProvider.providers[v.vp_idx];

        System.out.println(v.vp.country);
        System.out.println(v.load());

        Entry e = v.entries.getLast();

        Formatter f = new Formatter(System.out);
        f.format("entries: %s\napp date: %tc\nraw: %s\nts: %tc\nsd: %tc\nstatus: %s\n",
                v.entries.size(),
                v.appdate,
                e.raw_status,
                e.timestamp,
                e.status_date,
                e.status);
        f.flush();
    }

    /*
    private static class VFSHandler extends org.xml.sax.helpers.DefaultHandler
    {
        public String app_status = "";          // application status text
        public String app_date   = "";          // application submission date
        private boolean in_status = false;
        private boolean in_date   = false;

        @Override
        public void characters(char[] ch, int start, int length)
        {
            if (in_status)
                app_status += new String(ch, start, length);
            else if (in_date)
                app_date += new String(ch, start, length);
        }

        @Override
        public void startElement(String uri, String localName, String qName,
                                 org.xml.sax.Attributes attributes)
        {
            if (qName.equalsIgnoreCase("span")) {
                String id = attributes.getValue("id");
                if (id != null)
                    if (id.equals("lblScanStatus"))
                        in_status = true;
                    else if (id.equals("lblAppDate"))
                        in_date = true;
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName)
        {
            in_status = false;
            in_date = false;
        }
    }
    */

    public static class VFSException extends Exception
    {
    }

    public static class FormatException extends Exception
    {
        public final int id;

        public FormatException(int resource_id)
        {
            super();
            id = resource_id;
        }
    }
}
