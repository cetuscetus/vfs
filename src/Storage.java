/*
 * Copyright 2013 Nikita Schmidt
 *
 * This file is part of VFSCheck.
 *
 * VFSCheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VFSCheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VFSCheck.  If not, see <http://www.gnu.org/licenses/>.
 */

package vfs.check;

import android.content.*;
import java.util.Date;
import java.io.*;
import android.util.Log;

public final class Storage
{
    private static VisaStatus v;

    private static final String pref_file = "vfs.check.pref";   // legacy

    private static final int TAG_TYPE_STRING    = 0,
                             TAG_TYPE_BOOL      = 1,
                             TAG_TYPE_INT16     = 2,
                             TAG_TYPE_INT       = 3,
                             TAG_TYPE_INT64     = 4,
                             TAG_TYPE_STRUCT    = 5,

                             REFDATE_TAG    = 1 << 3,
                             LASTNAME_TAG   = 2 << 3,
                             APPDATE_TAG    = 3 << 3,
                             END_ENTRY_TAG  = 4 << 3 | TAG_TYPE_STRUCT,
                             STATUS_TAG     = 5 << 3,
                             TIMESTAMP_TAG  = 6 << 3 | TAG_TYPE_INT64,
                             DONE_TAG       = 7 << 3 | TAG_TYPE_STRUCT;

    private static VisaStati vv;

    public static VisaStati read(Context context)
    {
        if (vv != null)
            return vv;

        try {
            read_from_storage(context);
        } catch (IOException e) {
            throw new IOError(e);
        }

        return vv;
    }

    private static void read_from_storage(Context context) throws IOException
    {
        vv = new VisaStati();

        File dir = context.getFilesDir();
        Log.d("vfs.check", "dir '" + dir + "'");

        // load visa applications
        for (String file: context.fileList()) {
            Log.d("vfs.check", "file '" + file + "'");
            DataInputStream input = null;
            // if file is numeric, assign its name to key;
            // otherwise, ignore it.
            try {
                Integer key = new Integer(file);
                input = new DataInputStream(context.openFileInput(file));
                VisaStatus v = read_binary(input);
                v.updated = new Date(new File(dir, file).lastModified());
                vv.put(key, v);
            } catch (Exception e) {
            }
            if (input != null)
                input.close();
        }

        if (context.fileList().length == 0) {
            // migrate old SharedPreferences-based data, if present
            DataOutputStream output = null;
            try {
                VisaStatus v = read_from_old_prefs(context);
                Log.d("vfs.check", "old ShPref data found: " + v.reference);
                output = new DataOutputStream(context.openFileOutput("1", context.MODE_PRIVATE));
                write_new(output, v);
                Log.d("vfs.check", "data migrated");
                vv.put(1, v);
                // clear old file in case we can't remove it
                context.getSharedPreferences(pref_file, context.MODE_PRIVATE)
                        .edit().clear().commit();
                // attempt to remove ../shared_prefs/<pref_file>
                boolean d = new File(dir, "../shared_prefs/" + pref_file).delete();
                Log.d("vfs.check", d ? "old file deleted" : "error removing old file");
            } catch (Exception e) {
            }
            if (output != null)
                output.close();
        }
    }

    private static VisaStatus read_binary(DataInput input) throws Exception
    {
        int provider = input.readShort();
        String ref = input.readUTF();
        if (input.readByte() != REFDATE_TAG)
            throw new Exception();

        VisaStatus vs = new VisaStatus(ref, input.readUTF());

        vs.vp_idx = provider;
        vs.vp = VisaProvider.providers[provider];

        String status = null;
        long timestamp = 0;

        for (;;) {
            byte tag;
            try {
                tag = input.readByte();
            } catch (java.io.EOFException e) {
                break;
            }
            switch (tag) {
                case APPDATE_TAG:
                    vs.appdate = vs.parse_date_from_ui(input.readUTF());
                    break;
                case DONE_TAG:
                    vs.done = true;
                    break;
                case STATUS_TAG:
                    status = input.readUTF();
                    break;
                case TIMESTAMP_TAG:
                    timestamp = input.readLong();
                    break;
                case END_ENTRY_TAG:
                    vs.entries.addLast(new VisaStatus.Entry(status, new Date(timestamp), vs));
                    break;

                default:
                    tag &= 7;   // get type of this unknown tag
                    if (tag == TAG_TYPE_STRING)
                        input.readUTF();
                    else if (tag < TAG_TYPE_STRUCT)
                        input.skipBytes(1 << (tag - 1));
                    break;
            }
        }

        return vs;
    }

    // Write new application to output (which is a new binary file).
    private static void write_new(DataOutput output, VisaStatus v) throws IOException
    {
        output.writeShort(v.vp_idx);
        output.writeUTF(v.reference);
        output.writeByte(REFDATE_TAG);
        output.writeUTF(v.date_to_string(v.birthday));

        for (VisaStatus.Entry e: v.entries)
            write_entry(output, e, v);
    }

    // Append new entry to output.
    // If this is the first entry, also write the application date if known.
    // If this is currently the last entry, also write the last updated time.
    private static void write_entry(DataOutput output, VisaStatus.Entry e, VisaStatus v)
            throws IOException
    {
        if (v.appdate != null && e == v.entries.getFirst()) {
            output.writeByte(APPDATE_TAG);
            output.writeUTF(v.date_to_string(v.appdate));
        }

        output.writeByte(STATUS_TAG);
        output.writeUTF(e.raw_status);
        output.writeByte(TIMESTAMP_TAG);
        output.writeLong(e.timestamp.getTime());
        output.writeByte(END_ENTRY_TAG);

        if (v.done)
            output.writeByte(DONE_TAG);
    }

    private static VisaStatus read_from_old_prefs(Context context) throws Exception
    {
        SharedPreferences p = context.getSharedPreferences(pref_file, context.MODE_PRIVATE);

        if (!p.contains("reference"))
            throw new Exception();

        VisaStatus vs = new VisaStatus(p.getString("reference", ""),
                                       p.getString("birthday", ""));

        vs.vp_idx = p.getInt("provider", 0);
        vs.vp = VisaProvider.providers[vs.vp_idx];

        if (p.contains("appdate"))
            vs.appdate = vs.parse_date_from_ui(p.getString("appdate", ""));

        if (p.contains("updated"))
            vs.updated = new Date(p.getLong("updated", 0));

        vs.error = p.getInt("error", 0);

        for (int i = 0; ; i++) {
            String tag = Integer.toString(i);
            if (!p.contains("s" + tag))
                break;
            VisaStatus.Entry e = new VisaStatus.Entry(
                    p.getString("s" + tag, ""),
                    new Date(p.getLong("t" + tag, 0)),
                    vs
            );
            vs.entries.addLast(e);
        }

        vs.check_if_done();

        return vs;
    }

    // Add a new VisaStatus for tracking.
    // Return the number of visa applications being tracked.
    public static int add(Context context, VisaStatus v)
    {
        Integer n;
        if (read(context).isEmpty())
            n = new Integer(1);
        else {
            n = read(context).lastKey();
            // FIXME: temporarily this works like a 'replace'
            delete(context, n);
            //n = new Integer(n + 1);
            n = new Integer(1);
        }
        try {
            DataOutputStream output = new DataOutputStream(context.openFileOutput(n.toString(),
                    context.MODE_PRIVATE));
            write_new(output, v);
            output.close();
        } catch (IOException e) {
            throw new IOError(e);
        }
        vv.put(n, v);
        return vv.size();
    }

    // Delete a VisaStatus.
    public static void delete(Context context, int idx)
    {
        context.deleteFile(Integer.toString(idx));
        vv.remove(idx);
    }

    public static void update(Context context, int idx, VisaStatus v, boolean new_info)
    {
        try {
            if (new_info) {
                DataOutputStream output = new DataOutputStream(context.openFileOutput(
                            Integer.toString(idx), context.MODE_APPEND));
                write_entry(output, v.entries.getLast(), v);
                output.close();
            } else if (v.done) {
                DataOutputStream output = new DataOutputStream(context.openFileOutput(
                            Integer.toString(idx), context.MODE_APPEND));
                output.writeByte(DONE_TAG);
                output.close();
            } else {
                File file = new File(context.getFilesDir(), Integer.toString(idx));
                file.setLastModified(v.updated.getTime());
            }
        } catch (IOException e) {
            throw new IOError(e);
        }
    }

    public static void write_UNUSED(Context context, VisaStatus vnew, boolean with_entry)
    {
        SharedPreferences p = context.getSharedPreferences("0", context.MODE_PRIVATE);

        SharedPreferences.Editor editor = p.edit();

        if (vnew != v) {
            editor.clear();
            editor.putString("reference", vnew.reference);
            editor.putString("birthday", vnew.date_to_string(vnew.birthday));
            editor.putInt("provider", vnew.vp_idx);
        }
        if ((vnew != v || !p.contains("appdate")) && vnew.appdate != null)
            editor.putString("appdate", vnew.date_to_string(vnew.appdate));

        v = vnew;

        if (v.updated != null)
            editor.putLong("updated", v.updated.getTime());

        editor.putInt("error", v.error);

        if (with_entry) {
            String tag = Integer.toString(v.entries.size() - 1);
            VisaStatus.Entry e = v.entries.getLast();
            editor.putString("s" + tag, e.raw_status);
            editor.putLong("t" + tag, e.timestamp.getTime());
        }

        editor.apply();
    }
}
