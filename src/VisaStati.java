/*
 * Copyright 2013 Nikita Schmidt
 *
 * This file is part of VFSCheck.
 *
 * VFSCheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VFSCheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VFSCheck.  If not, see <http://www.gnu.org/licenses/>.
 */

package vfs.check;

import java.util.TreeMap;

// A collection of visa applications.
public final class VisaStati extends TreeMap<Integer, VisaStatus>
{
    // compare registration details
    public boolean same_as(VisaStati v)
    {
        if (equals(v))
            return true;
        if (size() != v.size())
            return false;

        for (Entry<Integer, VisaStatus> e: entrySet())
            if (!e.getValue().same_as(v.get(e.getKey())))
                return false;

        return true;
    }

    public boolean has_same_as(VisaStatus v)
    {
        for (VisaStatus vs: values())
            if (vs.same_as(v))
                return true;
        return false;
    }

    public boolean all_done()
    {
        for (VisaStatus vs: values())
            if (!vs.done)
                return false;
        return true;
    }
}
