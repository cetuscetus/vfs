/*
 * Copyright 2013 Nikita Schmidt
 *
 * This file is part of VFSCheck.
 *
 * VFSCheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VFSCheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VFSCheck.  If not, see <http://www.gnu.org/licenses/>.
 */

package vfs.check;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;
import android.content.Intent;
import android.net.Uri;

public final class AboutActivity extends Activity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(android.os.Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);

        String version;
        try {
            version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (Exception exc) {
            return;
        }
        ((TextView) findViewById(R.id.about_ver))
                .setText(getString(R.string.version) + " " + version);

        //AboutAdapter adapter = new AboutAdapter();
        //((ListView) findViewById(R.id.list)).setAdapter(adapter);
        //adapter.notifyDataSetChanged();
    }

    public void licence(View view)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("GPL.html"),
                                   this, InfoActivity.class);
        intent.putExtra("title", R.string.licence);
        startActivity(intent);
    }

    public void privacy(View view)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("Privacy.html"),
                                   this, InfoActivity.class);
        intent.putExtra("title", R.string.privacy_policy);
        startActivity(intent);
    }

    public void source(View view)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://bitbucket.org/cetuscetus/vfs/"));
        startActivity(intent);
    }

    /*
    static final class AboutAdapter extends BaseAdapter
    {
        private ViewGroup layout;

        public AboutAdapter(Activity a)
        {
            super();
            layout = (ViewGroup) a.getLayoutInflater().inflate(R.layout.about_item, null);
        }

        public int   getCount()             { return 20; }
        public Object getItem(int position) { return null; }
        public long getItemId(int position) { return position; }
        public int getItemViewType (int position) { return position == 0 ? 0 : 1; }
        public int getViewTypeCount () { return 2; }
        public boolean hasStableIds() { return true; }
        public boolean isEmpty() { return false; }

        public View getView(int position, View cv, ViewGroup parent)
        {
            if (cv == null) {
                ViewGroup v = (ViewGroup) View.inflate(parent.getContext(), R.layout.about_item, null);
                ((TextView) v.getChildAt(0)).setText("Item " + position);
                ((TextView) v.getChildAt(1)).setText("This is text at position " + position + ".");
                if ((position & 3) == 3)
                    ((TextView) v.getChildAt(1)).setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, R.drawable.gplv3, 0);
                return v;
            }
            return cv;
        }
    }
    */
}
