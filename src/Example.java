/*
 * Copyright 2013 Nikita Schmidt
 *
 * This file is part of VFSCheck.
 *
 * VFSCheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VFSCheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VFSCheck.  If not, see <http://www.gnu.org/licenses/>.
 */

package vfs.check;

import java.util.*;

public final class Example
{
    public static final VisaStatus get_visa_status()
    {
        VisaStatus v;

        try {
            v = new VisaStatus("SPAC/200513/1234/01", "11/12/1234");
        } catch (Exception e) {
            return null;
        }

        v.vp_idx = 2;
        v.vp = VisaProvider.providers[v.vp_idx];
        v.appdate = new GregorianCalendar(2013, 5 - 1, 20);
        v.updated = new Date();

        v.entries.addLast(new VisaStatus.Entry(
            "Your visa application is delivered to The Embassy of Czech " +
            "Republic in Moscow. Please continue to track the status of " +
            "your application on this site for further updates.",
            new Date(v.appdate.getTimeInMillis() + 86400000),
            v
        ));
        v.entries.addLast(new VisaStatus.Entry(
            "Your visa application has been processed and reviewed by The " +
            "Embassy of Czech Republic in Moscow and is in transit. Not "   +
            "ready for collection. Please continue to track the status of " +
            "your application on this site for further updates.",
            new Date(v.appdate.getTimeInMillis() + 7 * 86400000),
            v
        ));
        v.entries.addLast(new VisaStatus.Entry(
            "Your visa application has been processed and reviewed by The " +
            "Embassy of Czech Republic in Moscow and is ready for "         +
            "collection at the Visa service centre in ST PETERSBURG on "    +
            "28/05/2013. Please continue to track the status of "           +
            "your application on this site for further updates.",
            new Date(v.appdate.getTimeInMillis() + 8 * 86400000),
            v
        ));
        v.entries.addLast(new VisaStatus.Entry(
            "Your application has been picked up from the Visa service " +
            "Centre in ST PETERSBURG by yourself or your designated person",
            new Date(v.appdate.getTimeInMillis() + 8 * 86400000),
            v
        ));

        return v;
    }
}
