/*
 * Copyright 2013-2014, 2016 Nikita Schmidt
 *
 * This file is part of VFSCheck.
 *
 * VFSCheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VFSCheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VFSCheck.  If not, see <http://www.gnu.org/licenses/>.
 */

package vfs.check;

public final class VisaProvider
{
    public static final VisaProvider[] providers = {
        // index 0: disabled (search starts at 2)
        new VisaProvider(R.string.uk, 0, //R.drawable.uk,
            "https://www.vfsglobal.org/ukg-passporttracking/"
              + "applicanttrackstatus1.aspx?Data=YzLYMFd3w3LznOCbxa6ahQ%3d%3d",
            "__VIEWSTATE=aDFiNpIzkB9osYlGOY0QG2wS%%2FFqlJ4%%2Fv%%2F2vNcxKwq3TW"
              + "EKoUJM1JR3eTzfv9nsOca%%2FDJf59fY85OyrsaVv%%2BBNqCHO4ygTSA7iCO"
              + "FqKpzM2SLequUlfMQRrgQmjV6CYTlMb%%2Bymwg%%2BDMSw14I%%2BU6b6fIf"
              + "wwAM%%3D&"
              + "__VIEWSTATEENCRYPTED=&"
              + "__EVENTVALIDATION=aYd3LEH1FtUKC9wuQukPznBOuVmNRpXB9eq6IYUxHtr"
              + "Us4AlkHaE61zNss52Mrtg4G9Xgyol6GnNzvhNqsEL7KEYkzWhrw6MtutqKoeC"
              + "sFbX1%%2FwsOJKZFIv38nL6avinPTP40w%%3D%%3D&"
              + "txtRefNO=%s%%2F%s%%2F%s%%2F%s&"
              + "txtDat=%td&txtMont=%tm&txtYea=%tY&"
              + "cmdSubmit=Submit",
            "id(\"lblAppDate\")",
            "id(\"lblScanStatus\")",
            true
        ),
        // index 1: disabled (search starts at 2)
        // This one needs a cookie.
        new VisaProvider(R.string.fi, 0, //R.drawable.fi,
            "https://www.vfsrussiavisaservices.com/finland-russia-tracking/"
              + "trackingparam.aspx",
            "__EVENTTARGET=&__EVENTARGUMENT=&"
              + "__VIEWSTATE=%%2FwEPDwUKMTY0NjkzNDM3OQ9kFgJmD2QWAgIDD2QWAgIBD2"
              + "QWDgIFDw8WBB4EVGV4dAUBKh4UVmFsaWRhdGlvbkV4cHJlc3Npb24FDl5cdys"
              + "oWy9dXHcrKSokZGQCCw8PFgQfAAUBKh8BBRReKFthLXpdfFtBLVpdfFsgXSkq"
              + "JGRkAg0PD2QWAh4Hb25jbGljawUfSGlkZUVycm9yKCdjdGwwMF9DUEhfZGl2R"
              + "XJyb3InKWQCEw8PFgQfAAUBKh8BBQ5eXHcrKFsvXVx3KykqJGRkAhUPZBYCZg"
              + "8PZBYIHwIFNWV2ZW50LmNhbmNlbEJ1YmJsZT10cnVlOyB0aGlzLnNlbGVjdCg"
              + "pOyBjYWxfbGNzKHRoaXMpHgpvbmtleXByZXNzBRdyZXR1cm4gY2FsX0NoZWNr"
              + "KGV2ZW50KR4Gb25ibHVyBRNjYWxfVmFsaWRhdGUodGhpcyk7HglvbmtleWRvd"
              + "24FKmlmKGV2ZW50LmtleUNvZGU9PTkpY2FsX2NoZWNrQ2xpY2soZXZlbnQpO2"
              + "QCFw8PZBYCHwIFH0hpZGVFcnJvcignY3RsMDBfQ1BIX2RpdkVycm9yJylkAh0"
              + "PFgIeCWlubmVyaHRtbAVCTm8gZGV0YWlscyBmb3VuZCBtYXRjaGluZyB3aXRo"
              + "IHRoZSByZWZlcmVuY2Ugbm8gYW5kIGRhdGUgb2YgYmlydGguZGQPtCuo5Ly43"
              + "leyXuPzdu6niD%%2BJlA%%3D%%3D&"
              + "__EVENTVALIDATION=%%2FwEWBwK7146pBAKxutWeBQL3x9r6DgKS3ezXCAKn"
              + "ld7qBwL3m8e3AwKLz8%%2BYBlImjNQPFWWRfz1BSv1fJMQgNf5M&"
              + "ctl00%%24CPH%%24txtRef=&"
              + "ctl00%%24CPH%%24txtLname=&"
              + "ctl00%%24CPH%%24txtRef2=%s%%2F%s%%2F%s%%2F%s&"
              + "ctl00%%24CPH%%24txtDOB%%24txtDate=%td%%2F%tm%%2F%tY&"
              + "ctl00%%24CPH%%24btnDOB=Submit",
            null,
            "//td[b=\"Your Passport Status\"]/following::table[1]/descendant::td[1]",
            false
        ),
        /*
        new VisaProvider(R.string.fi, 0, //R.drawable.fi,
            "https://www.visaservices.firm.in/finland-russia-tracking/"
              + "trackingparam.aspx",
            "__LASTFOCUS=&__EVENTTARGET=&__EVENTARGUMENT=&"
              + "__VIEWSTATE=%%2FwEPDwUKMTY0NjkzNDM3OQ9kFgJmD2QWAgIDD2QWAgIBD2"
              + "QWDAIFDw8WAh4UVmFsaWRhdGlvbkV4cHJlc3Npb24FDl5cdysoWy9dXHcrKSo"
              + "kZGQCCw8PFgIfAAUUXihbYS16XXxbQS1aXXxbIF0pKiRkZAINDw9kFgIeB29u"
              + "Y2xpY2sFH0hpZGVFcnJvcignY3RsMDBfQ1BIX2RpdkVycm9yJylkAhMPDxYCH"
              + "wAFDl5cdysoWy9dXHcrKSokZGQCFQ9kFgJmDw9kFggfAQU1ZXZlbnQuY2FuY2"
              + "VsQnViYmxlPXRydWU7IHRoaXMuc2VsZWN0KCk7IGNhbF9sY3ModGhpcykeCm9"
              + "ua2V5cHJlc3MFF3JldHVybiBjYWxfQ2hlY2soZXZlbnQpHgZvbmJsdXIFE2Nh"
              + "bF9WYWxpZGF0ZSh0aGlzKTseCW9ua2V5ZG93bgUqaWYoZXZlbnQua2V5Q29kZ"
              + "T09OSljYWxfY2hlY2tDbGljayhldmVudCk7ZAIXDw9kFgIfAQUfSGlkZUVycm"
              + "9yKCdjdGwwMF9DUEhfZGl2RXJyb3InKWRkuW92cQ7H52g%%2Ff9Yis%%2BnCN"
              + "pUI7M0%%3D&"
              + "__EVENTVALIDATION=%%2FwEWBwLv8rvuDAKxutWeBQL3x9r6DgKS3ezXCAKn"
              + "ld7qBwL3m8e3AwKLz8%%2BYBle6qQTYMiRfOD1x5ydCO%%2B7Lq2My&"
              + "ctl00%%24CPH%%24txtRef=&"
              + "ctl00%%24CPH%%24txtLname=&"
              + "ctl00%%24CPH%%24txtRef2=%s%%2F%s%%2F%s%%2F%s&"
              + "ctl00%%24CPH%%24txtDOB%%24txtDate=%td%%2F%tm%%2F%tY&"
              + "ctl00%%24CPH%%24btnDOB=Submit",
            null,
            "//td[b=\"Your Passport Status\"]/following::table[1]/descendant::td[1]",
            false
        ),
        */
        new VisaProvider(R.string.cz, 0, //R.drawable.cz,
            "https://www.visaservices.firm.in/Czech-Russia-Track/Russia/"
              + "TrackyourPassportRU.aspx",
            "__VIEWSTATE=dDwxNTEzMzQzMjQ2O3Q8O2w8aTwxPjs%%2BO2w8dDw7bDxpPDM%%2"
              + "BOz47bDx0PDtsPGk8MT47aTwzPjtpPDU%%2BOz47bDx0PHA8O3A8bDxvbktle"
              + "VByZXNzOz47bDxyZXR1cm4gUmVmTm9WYWxpZGF0aW9uKClcOzs%%2BPj47Oz4"
              + "7dDxwPDtwPGw8b25CbHVyOz47bDxyZXR1cm4gY2hrQmlydGhEYXRlKHRoaXMp"
              + "XDs7Pj4%%2BOzs%%2BO3Q8cDw7cDxsPG9uQ2xpY2s7PjtsPHJldHVybiBWYWx"
              + "pZGF0ZUJ0blN1Ym1pdDEoKVw7Oz4%%2BPjs7Pjs%%2BPjs%%2BPjs%%2BPjs%"
              + "%2BZ7b2mYYIRUKVJ516Uw2V%%2FbRiXKw%%3D&"
              + "txtref1=%s%%2F%s%%2F%s%%2F%s&txtdob3=%td%%2F%tm%%2F%tY&"
              + "BtnSubmit1=Submit",
            "id('lblsubm_date')",
            "concat(id('lblPPStatus'), '\n', id('lblPPStatusRU'))",
            false
        ),
        new VisaProvider(R.string.pl, 0, //R.drawable.pl,
            "https://www.vfsvisaservices-russia.com/poland-Russia-tracking_new/"
              + "trackingParam.aspx?p=ri7FHohe3VirNKmyLaRu36t9%2fpEItw3gfYXFtDFlxVY%3d",
            "__LASTFOCUS=&__EVENTTARGET=&__EVENTARGUMENT=&"
              + "__VIEWSTATE=cDG3EP%%2BCPvmIuQIwALX%%2BMCh4g7VBUx%%2FVan8um56V5Q1bbfy3o%%2BChC27vB"
              + "t6%%2BxJVwRdtfKrerPVrDK9C%%2FtF1csyFsnCVvHXB0TH3U1%%2BiSTR1OU5mUCPPUrPN3KG3sV57AI"
              + "TfFJFMhIhyTgfi5gWFdYXJgsR%%2F%%2F9jcVJ7F5BcOy304DeSOMGlNIG%%2FsBK4tj5GXoX5JVRPcQC"
              + "kjbtzQ5gVBB%%2FwvGnsYUTNav4MZGlKkl1aUD72fw3Q8WhQgShDOP%%2Fk4SHBt5PUrkreqyZLA8oFQA"
              + "GQmLDnYAISmvr3Gd3h0Kocm5zPmjr13l8ldqhRb9KPYHAhO7QjU5FZq5msunbeSOWl4opCQHJYS5NUKxN"
              + "R5ZhnSHB2Bm1U5CyKBh4WXZ%%2Bb9Wcn3nkOM3SWvqesPWdWp9KAjoPGpxXNRIti%%2FkIC4Zp%%2BPNR"
              + "TY9f%%2BIdvcv30tHuIJchaG7p3GC9pPQFIBLRQ0Qqxd7G4a8PU%%2FwTlEaJRA%%3D%%3D&"
              + "__VIEWSTATEGENERATOR=9AC01ED4&"
              + "__EVENTVALIDATION=YcJ2m1FMHF7Y5jKtfb0nRluKHhdUO2IrrQWwBM%%2BmBw8QwHK8teO8lbcxTTJ4"
              + "E19r%%2Book6xEX3fM3kga%%2BoiZ3wCrP21FswrFi9oSqoZyHsILGVSF0G%%2Be5YnYNPsEJ4S22tD2u"
              + "xbW7S8oz7uz%%2FrPorme5BsdM%%3D&"
              + "ctl00%%24CPH%%24txtR2Part1=%s&"
              + "ctl00%%24CPH%%24txtR2Part2=%s&"
              + "ctl00%%24CPH%%24txtR2Part3=%s&"
              + "ctl00%%24CPH%%24txtR2Part4=%s&"
              + "ctl00%%24CPH%%24txtDOB%%24txtDate=%td%%2F%tm%%2F%tY&"
              + "ctl00%%24CPH%%24btnDOB=Submit",
            null,
            "//td[@class='fnstatus']//td",
            false
        ),
        /*
        new VisaProvider(R.string.pl, 0, //R.drawable.pl,
            "https://www.visaservices.firm.in/poland-russia-tracking_new/"
              + "TrackingParam.aspx?p=ri7FHohe3VirNKmyLaRu36t9%2fpEItw3gfYXFtDFlxVY%3d",
            "__EVENTTARGET=&__EVENTARGUMENT=&"
              + "__VIEWSTATE=1nAnYqpAtJBlwlPZXDVrcesHj6BbWceXPYy2Yrwo7FtYPFdOfYx06HndpQfd6ifR4Ni7%"
              + "%2BAjgYiOHVTZdqX3824DImwCfWZ%%2BYPC9ETQO2OQ%%2Bu7jjPfza1QMeNgxycGwewhde6LujwvSU3"
              + "iNac0ohaUPX9KYKGt7Xi3YUgjkPuN8PfbAgPL3UAzLIUaSDQzDi6DejOVH1mgjlxDQLTeZH3SSWY0xrs8"
              + "mJHL7cS0DnNWv9ShrU%%2B4GvGDNf0DUJdcEwMNizU3xLfkzOhj3SFd%%2B9J0mRDTm2JGmZMoTcjj5%%"
              + "2F%%2B8iwrhNPPx0V3QODQ3dQBf3ZOJIS6DddHXK0BUSU7n5JCaqm07I5774WHhdwfNPSRh%%2FGQFNwX"
              + "9dvA8R1BlWEOMbUO6ubd7X4%%2FPkzMDyijlXgkI2uNZjpGYorzn6Qh%%2B%%2BGGAEXarEfBlPNJ7Txz"
              + "q0TgwbrcIcQLg2AqEzcZ%%2BNquinqEeLDVpM1mq0ymVsg7V97HObM1IdT%%2FVz72qO5v%%2B9g1mH%%"
              + "2FXMBo%%2BLABELvsTgnT65ezi%%2FSfAt59%%2BezkAWULMOPxuSEo%%3D&"
              + "ctl00%%24CPH%%24txtR2Part1=%s&"
              + "ctl00%%24CPH%%24txtR2Part2=%s&"
              + "ctl00%%24CPH%%24txtR2Part3=%s&"
              + "ctl00%%24CPH%%24txtR2Part4=%s&"
              + "ctl00%%24CPH%%24txtDOB%%24txtDate=%td%%2F%tm%%2F%tY&"
              + "ctl00%%24CPH%%24btnDOB=Submit&"
              + "__EVENTVALIDATION=swMHTWA15CMZX2buRprTY44bM4Wq79i92Tdcrts%%2BQIkqUNeDBJVPADxBhIdT"
              + "P%%2BAcbhb%%2BRi3wOcREqYk3FSrMsKtXVzkxbCRs",
            null,
            "//td[@class='fnstatus']//td",
            false
        ),
        */
        new VisaProvider(R.string.es, 0, //R.drawable.es,
            "https://www.visaservices.firm.in/Spain-Russia-Tracking/Track.aspx",
            "__EVENTTARGET=&__EVENTARGUMENT=&"
              + "__VIEWSTATE=dDw0MTIwNjA4MzE7dDw7bDxpPDE%%2BOz47bDx0PDtsPGk8Mz47aTw1PjtpPDExPjs%%2"
              + "BO2w8dDx0PDs7bDxpPDA%%2BOz4%%2BOzs%%2BO3Q8cDxwPGw8TWF4TGVuZ3RoOz47bDxpPDUwPjs%%2B"
              + "Pjs%%2BOzs%%2BO3Q8cDxwPGw8VGV4dDs%%2BO2w8RGF0ZSBvZiBiaXJ0aC47Pj47Pjs7Pjs%%2BPjs%%"
              + "2BPjs%%2BjSoUSbS%%2FR2MknfBXLc7PuWEAssA%%3D&"
              + "rbList=1&"
              + "txtAppref=%s%%2F%s%%2F%s%%2F%s&"
              + "txtDOB=%td%%2F%tm%%2F%tY&"
              + "btnSubmit=Submit",
            null,
            "id('lblStatus')",
            false
        ),
        new VisaProvider(R.string.uk, 0, //R.drawable.uk,
            "https://www.vfs.org.in/UKG-PassportTracking/"
              + "ApplicantTrackStatus.aspx?Data=mrXgl7jN6EAAmAlbO9YHBQ%3d%3d",
            "__VIEWSTATE=3UfJzWflvuBBlggXqBy3r0918f8ExSgz5S26A5LApESMyTYj5Q4o7"
              + "CUwv8XcoveZPyOZWl%%2FuDHQwZQzBuCHyfQ%%3D%%3D&"
              + "__VIEWSTATEENCRYPTED=&"
              + "__EVENTVALIDATION=4P69S8my%%2FT1zZAnwR%%2FVDtkp5UZvb98iT0Bgcj"
              + "ZoJSTeANf6Z%%2FKrmTbdH6WahEWvRfnpoStr7qcBEibyl%%2BcQVKw%%3D%%3D&"
              + "txtRefNO=%s%%2F%s%%2F%s%%2F%s&"
              + "txtDat=%td&txtMont=%tm&txtYea=%tY&"
              + "cmdSubmit=Submit",
            "id(\"lblAppDate\")",
            "id(\"lblScanStatus\")",
            true
        ),
        /*
        new VisaProvider(R.string.de, 0,
            "https://www.vfsvisaservices.com/Germany-Russia-tracking/TrackingParam.aspx?"
              + "p=221$7$44$21$227$195$228$120$190$155$9$95$175$240$109$95$90$172$198$43$1$212$179"
              + "$116$149$53$6$34$89$5$198$164",
            "__LASTFOCUS=&__EVENTTARGET=&__EVENTARGUMENT=&"
              + "__VIEWSTATE=ghHHJucNOFI3Pyc24JHsD8an5vgfLC0diEQE%%2Ftyg2oIKSfqq41fDfrtdb8AdGqb3JdaIQZDNsKB%%2Fo4hzqkRbWAeTeMXr36rEw9MEw%%2BC5igOeBp2bK%%2B34uINzeMPki66gIBo%%2B4kUwcqUptK%%2BDpSWD65jgpMfl48jriWcpwH7dvZOzJsyEIKhOht%%2BX38yUED%%2BIi2lI%%2B9kexfrtP0N%%2BCNK80%%2BTBXK2lzTau02kmFXZJJJTGrsUNSWE43VoYEaryWCYr8TgYtsJxAy1Wqqb%%2Bpdv%%2F0amPBx01mFPKEsn%%2BAVduQI8u7CFv13YmcziJ%%2F%%2FgjHMAQ%%2FtigU2pZEAUHf4Q0bKKPx20onHhEZU7G27Ax2c6JOcwpNFumMWg8F9CkQ0WRzpZVgc2FmN9mPZ2mrza8aF%%2F085Dez0e1vvKqIh8%%2BhJY%%2B1O%%2Fkdwi76cWjwFmhTaWRC05W7%%2FigYgb2XzdLj4550qnBZcSFJ07jv3Sa5yuw%%2BJAbMKsg4AgSFrHYogMntOw%%3D&"
              + "__EVENTVALIDATION=FHqncBMBSNlUoBIxnJ4Z81Va76zeU4MWz%%2F2hKwMWWrLoqR7xm6oVavZDaulSjISX1131w1jjPD7zYrk7qpLdxVCQ2fNnCQYzSFdZI%%2BZyKI75XOo79Sa9K9HcsURoXLwJmk%%2BjfDUT7CALuQohzsn32F72nrfmhqYx0EQr6g%%3D%%3D&"
              + "ctl00%%24hidCSRF=TrackingParam.aspx9dd9a03e-1055-47d7-a971-06f59c96954d&"
              + "ctl00%%24CPH%%24txtR2Part1=%s&"
              + "ctl00%%24CPH%%24txtR2Part2=%s&"
              + "ctl00%%24CPH%%24txtR2Part3=%s&"
              + "ctl00%%24CPH%%24txtR2Part4=%s&"
              + "ctl00%%24CPH%%24txtDOB%%24txtDate=%td%%2F%tm%%2F%tY&"
              + "ctl00%%24CPH%%24btnDOB=%%D0%%BE%%D1%%82%%D1%%81%%D0%%BB%%D0%%B5%%D0%%B4%%D0%%B8%%D1%%82%%D1%%8C+%%D1%%81%%D1%%82%%D0%%B0%%D1%%82%%D1%%83%%D1%%81",
            null,
            "//td[@class='fnstatus']//td",
            false
        ),
        */
        new VisaProvider(R.string.pl, 0, //R.drawable.pl,
            "https://polandonline.vfsglobal.com/Poland-Belarus-Tracking/"
              + "TrackingParam.aspx?P=X98jcslIevfe3MYiYk4%2fq71D3LIPK7luCeBIxRr68No%3d",
            "__LASTFOCUS=&__EVENTTARGET=&__EVENTARGUMENT=&"
              + "__VIEWSTATE=JVkDXPF%%2F2CWwlG1Lz%%2F%%2B0w7KsuhPwRTW4jOCxZolLmjNG8t76cFsHG%%2B%%2"
              + "FnMfYD0p7zVGzhkTdoVn5bwVghEo1UF6Tejn%%2B8dWCKeTkqupto%%2BdLezLonclfyGRQm94EC4Xm1W"
              + "TL1FXEEarVnnDlB9mwAsBtHyiG%%2B2msT3aKc77yeqcEVmATx7gvk9ld3Rd3ng5R2zBbJ5qw0Vw1ieYd"
              + "A5qSHgyVjhRW13JMm%%2FzNUIw1h6azn9sJl13YaLlUzs2%%2F2hW460Ye2F%%2FLjJGZVjpBOMvrREHL"
              + "ptu95e1xxE9JnCTQSx9p1h4hJJPBf5BI0vIw%%2Brv8nzTE9ZFN2jwj0KAUJJWrZ2wHvFPOIeYVFFT8Dp"
              + "vlKLujv5tcwY892e6k1Kv5aVw%%2F5cHFASD1F1Ym9h0awiZw3xzIaCs9oP0P9YGWJjTj0%%2FVZIY6ag"
              + "52%%2BW5uZo%%2BH1F%%2BaYh42legrzW%%2BlPUHuw3mAYn6zKuuukvctjWogK4Wg%%3D%%3D&"
              + "__EVENTVALIDATION=DkzMht4txPMxbAHNIUzOl%%2FIxL63gVjrmmjkHDV9hVd9lJgxOfweug%%2BjEB"
              + "ySUEFNkIwvvgLNvCBpRREjI7pqDRUr9aL%%2BDx1IoREc2Od14HM1d6kjPdg7h%%2BYtf7xZ2xmiAEv5G"
              + "UDy%%2BrNaYr1DQ1fbL5LTBHMA%%3D&"
              + "ctl00%%24CPH%%24txtR2Part1=%s&"
              + "ctl00%%24CPH%%24txtR2Part2=%s&"
              + "ctl00%%24CPH%%24txtR2Part3=%s&"
              + "ctl00%%24CPH%%24txtR2Part4=%s&"
              + "ctl00%%24CPH%%24txtDOB%%24txtDate=%td%%2F%tm%%2F%tY&"
              + "ctl00%%24CPH%%24btnDOB=%%D0%%9E%%D1%%82%%D0%%BF%%D1%%80%%D0%%B0%%D0%%B2%%D0%%B8%%"
              + "D1%%82%%D1%%8C",
            null,
            "//td[@class='fnstatus']//td",
            false
        ),
    };

    public final int     country;       // country name resource id
    public final int     flag;          // country flag resource id
    public final String  url;           // POST request URL
    public final String  post;          // POST request body format string
    public final String  appdate_xpath; // XPath expression for application date
    public final String  status_xpath;  // XPath expression for visa status
    public final boolean mdy;           // whether the day format is mm/dd/yyyy

    private VisaProvider(int c, int f, String u, String p, String a, String s,
                         boolean d)
    {
        country = c;
        flag = f;
        url = u;
        post = p;
        appdate_xpath = a;
        status_xpath = s;
        mdy = d;
    }
}
