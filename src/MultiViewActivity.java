/*
 * Copyright 2013 Nikita Schmidt
 *
 * This file is part of VFSCheck.
 *
 * VFSCheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VFSCheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VFSCheck.  If not, see <http://www.gnu.org/licenses/>.
 */

package vfs.check;

import android.app.Activity;
import android.app.ExpandableListActivity;
import android.content.*;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import android.graphics.Color;
import android.text.format.DateUtils;
import android.text.TextUtils;
import java.util.Formatter;
import java.util.Calendar;

public final class MultiViewActivity extends ExpandableListActivity
{
    public static Updater updater = new Updater();

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        VisaStati v = Storage.read(this);

        if (v.isEmpty()) {
            startActivity(new Intent(this, ConfigActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
            return;
        }

        updater.set_adapter(new MultiStatusAdapter(this, v));
        setListAdapter(updater.adapter);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        updater.set_adapter(null);
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // tell Notifier we don't want status bar notifications and
        // cancel any that may have been issued already
        Notifier.stop_status_bar(this);

        /*
        // ConfigActivity may have changed the details we are tracking
        VisaStati v = Storage.read(this);
        if (!v.same_as(updater.adapter.v))
            updater.draw_new(v);
        */
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        // tell Notifier to use the status bar for notifications
        Notifier.start_status_bar();
    }

    // Populate the action bar and overflow menu.
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        updater.menu_item = menu.findItem(R.id.menu_update);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.menu_update:
                Intent intent = new Intent("vfs.check.REFRESH", null, this,
                                           VFSBroadcastReceiver.class);
                sendBroadcast(intent);
                return true;

            case R.id.menu_config:
                startActivity(new Intent(this, ConfigActivity.class));
                //finish();
                return true;

            case R.id.menu_about:
                startActivity(new Intent(this, AboutActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static final class Updater
    {
        private volatile VisaStati visa_stati;
        public MultiStatusAdapter adapter;
        public MenuItem menu_item;

        // Update associated ViewActivity with new data.
        // Pass null to indicate that a refresh is in progress.
        // This may be called on any thread.
        public synchronized void show_on_ui_thread(final int idx, final VisaStatus v)
        {
            if (adapter != null)
                adapter.activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() { show_update(idx, v); }
                });
        }

        // Set or remove an adapter.
        public synchronized void set_adapter(MultiStatusAdapter a)
        {
            adapter = a;
        }

        public void show_update(int idx, VisaStatus v)
        {
            if (adapter == null)
                return;         // activity destroyed, nothing to do here

            if (v == null) {
                // refresh progress indication
                if (menu_item != null)
                    if (idx == -1)
                        menu_item.setActionView(R.layout.busy);
                    else
                        menu_item.setActionView(null);
                return;
            }

            adapter.notifyDataSetChanged();
        }
    }
}

final class MultiStatusAdapter extends BaseExpandableListAdapter
{
    public final Activity activity;
    public final VisaStati v;

    public MultiStatusAdapter(Activity a, VisaStati vi)
    {
        super();
        activity = a;
        v = vi;
    }

    // Gets the number of groups.
    // A group is one visa application, which is a single VisaStatus value in v.
    @Override
    public int getGroupCount() { return v.size(); }

    // Gets the data associated with the given group.
    @Override
    public final Object getGroup(int group_position)
    {
        int i = v.size();

        for (VisaStatus vs: v.values())
            if (--i == group_position)
                return vs;

        return null;
    }

    // Gets the number of children in a specified group.
    // All entries in a VisaStatus, except the last one, are children.
    // The last one is part of the group's View.
    @Override
    public int getChildrenCount(int group_position)
    {
        VisaStatus vs = (VisaStatus) getGroup(group_position);
        return vs.entries.size() - 1;
    }

    // Gets the data associated with the given child within the given group.
    @Override
    public Object getChild(int group_position, int child_position)
    {
        VisaStatus vs = (VisaStatus) getGroup(group_position);
        return vs.entries.get(vs.entries.size() - child_position - 2);
    }

    // Gets the ID for the group at the given position.
    // This is not a stable ID, because new groups (VisaStatus instances) are inserted at the top,
    // shifting existing groups' positions by one.
    // If there is any benefit from using stable IDs, this can be converted to a stable ID by
    // iterating through v (VisaStati).
    @Override
    public long getGroupId(int group_position)
    {
        return group_position;
    }

    // Gets the ID for the given child within the given group.
    // This is not a stable ID, because new status entries are inserted at the top, shifting
    // existing children's positions by one.
    // If there is any benefit from using stable IDs, this can be converted to a stable ID by
    // finding the group's VisaStatus vs and returning vs.entries.size() - child_position instead.
    @Override
    public long getChildId(int group_position, int child_position)
    {
        return child_position;
    }

    @Override
    public boolean isChildSelectable(int group_position, int child_position)
    {
        return false;
    }

    // Indicates whether the child and group IDs are stable across changes to the underlying data.
    // Returns whether or not the same ID always refers to the same object.
    @Override
    public boolean hasStableIds()
    {
        return false;
    }

    // Gets a View that displays the given group.
    public View getGroupView (int position, boolean is_expanded, View cv, ViewGroup parent)
    {
        EntryView latest_entry = null;

        // It is not guaranteed that cv will have been previously created by getGroupView.
        if (cv != null)
            latest_entry = (EntryView) cv.findViewById(android.R.id.text1);
        if (latest_entry == null) {
            cv = activity.getLayoutInflater().inflate(R.layout.group, parent);
            latest_entry = new EntryView(activity, Color.CYAN);
            latest_entry.setId(android.R.id.text1);
            ((ViewGroup) cv).addView(latest_entry);
        }

        VisaStatus vs = (VisaStatus) getGroup(position);

        ((TextView) cv.findViewById(R.id.reference))
            .setText(activity.getString(vs.vp.country) + ", " + vs.reference);

        Calendar c = vs.appdate;
        StringBuilder sb = new StringBuilder();
        if (c != null) {
            Formatter fmt = new Formatter(sb);
            fmt.format(activity.getString(R.string.appdate), c, c, c);
        }
        ((TextView) cv.findViewById(R.id.appdate)).setText(sb);

        ((TextView) cv.findViewById(R.id.error)).setText(
            vs.error == 0 ? "" : activity.getString(vs.error));

        ((TextView) cv.findViewById(R.id.lastupd)).setText(
            TextUtils.concat(
                activity.getString(R.string.lastupd),  // "Updated"
                " ",
                DateUtils.getRelativeTimeSpanString(activity, vs.updated.getTime(), true)
            )
        );

        latest_entry.fill(vs.entries.getLast());

        return cv;
    }

    // Gets a View that displays the data for the given child within the given group.
    public View getChildView (int group_position, int child_position, boolean is_last_child,
                              View cv, ViewGroup parent)
    {
        // It is not guaranteed that cv will have been previously created by getChildView.
        EntryView ev;

        if (cv instanceof EntryView)
            ev = (EntryView) cv;
        else
            ev = new EntryView(activity, Color.LTGRAY);

        VisaStatus vs = (VisaStatus) getGroup(group_position);
        ev.fill(vs.entries.get(vs.entries.size() - child_position - 2));
        return ev;
    }

    private static class EntryView extends LinearLayout
    {
        private TextView status_view;
        private TextView date_view;

        public EntryView(Context context, int colour)
        {
            super(context);
            setOrientation(VERTICAL);

            status_view = new TextView(context);
            date_view = new TextView(context);

            status_view.setTextColor(colour);
            date_view.setTextSize(11);
            date_view.setTextColor(Color.YELLOW);

            addView(status_view, new LayoutParams(LayoutParams.MATCH_PARENT,
                        LayoutParams.WRAP_CONTENT));
            LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT);
            lp.gravity = Gravity.RIGHT;
            addView(date_view, lp);
        }

        public void fill(VisaStatus.Entry e)
        {
            StringBuilder sb = new StringBuilder();
            Formatter fmt = new Formatter(sb);
            fmt.format("%ta, %te %tB", e.status_date, e.status_date, e.status_date);
            date_view.setText(sb);
            status_view.setText(e.status);
        }
    }
}
