/*
 * Copyright 2013 Nikita Schmidt
 *
 * This file is part of VFSCheck.
 *
 * VFSCheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VFSCheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VFSCheck.  If not, see <http://www.gnu.org/licenses/>.
 */

package vfs.check;

import android.app.IntentService;
import android.content.Intent;

public final class UpdateService extends IntentService
{
    public UpdateService()
    {
        super("UpdateService");
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        synchronized (UpdateService.class) {
            VisaStati vv = Storage.read(this);
            VisaStatus notifiable = null;
            int num_notifiables = 0;
            boolean requested_by_user = intent.getAction() != null;

            // show refresh in progress
            ViewActivity.updater.show_on_ui_thread(-1, null);

            for (VisaStati.Entry<Integer, VisaStatus> i: vv.entrySet()) {
                int idx = i.getKey();
                VisaStatus v = i.getValue();
                boolean new_info = false;

                if (v.done && !requested_by_user)
                    continue;

                try {
                    new_info = v.load();
                    v.error = 0;
                    Storage.update(this, idx, v, new_info);
                } catch (VisaStatus.VFSException e) {
                    v.error = R.string.vfsexception;
                } catch (Exception e) {
                    v.error = R.string.netexception;
                } finally {
                    if (new_info) {
                        notifiable = v;
                        num_notifiables++;
                    }
                    ViewActivity.updater.show_on_ui_thread(idx, v);
                }
            }

            ViewActivity.updater.show_on_ui_thread(0, null);

            if (num_notifiables != 0 && !requested_by_user)
                Notifier.notify(this, num_notifiables, notifiable);
        }
    }
}
