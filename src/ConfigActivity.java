/*
 * Copyright 2013 Nikita Schmidt
 *
 * This file is part of VFSCheck.
 *
 * VFSCheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VFSCheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VFSCheck.  If not, see <http://www.gnu.org/licenses/>.
 */

package vfs.check;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.AsyncTask;
import android.view.*;
import android.widget.*;
import android.text.InputFilter;
import android.text.Spanned;

public final class ConfigActivity extends Activity
{
    public VerifyTask task;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config);

        EditText ref = (EditText) findViewById(R.id.reference);
        EditText bday = (EditText) findViewById(R.id.date);

        if (ref.length() == 0 && bday.length() == 0) {
            VisaStati v = Storage.read(this);

            if (!v.isEmpty()) {
                VisaStatus vs = v.firstEntry().getValue();
                ref.setText(vs.reference);
                bday.setText(vs.date_to_string(vs.birthday));
            }
        }

        // Add a VACFilter to the list of ref's filters to monitor the VAC code
        // and modify bday accordingly.
        VACFilter filter = new VACFilter(bday, findViewById(R.id.submit_button));
        InputFilter[] filters = ref.getFilters();
        InputFilter[] new_filters = new InputFilter[filters.length + 1];
        System.arraycopy(filters, 0, new_filters, 0, filters.length);
        new_filters[filters.length] = filter;
        ref.setFilters(new_filters);
        filter.filter(null, 0, 0, ref.getText(), 0, 0);
    }

    public void submit(View button)
    {
        if (task != null)
            return;

        EditText ref = (EditText) findViewById(R.id.reference);
        EditText bday = (EditText) findViewById(R.id.date);
        VisaStatus v;

        try {
            v = new VisaStatus(ref.getText().toString(), bday.getText().toString());
        } catch (VisaStatus.FormatException e) {
            Toast.makeText(this, e.id, Toast.LENGTH_SHORT).show();
            return;
        }

        if (Storage.read(this).has_same_as(v)) {
            // details unchanged
            Intent intent = new Intent("vfs.check.REFRESH", null, this, VFSBroadcastReceiver.class);
            sendBroadcast(intent);
            startActivity(new Intent(this, ViewActivity.class));
            return;
        }

        if (BuildConfig.DEBUG) {
            // check for special year; Calendar.YEAR is 1
            if (v.birthday.get(1) == 1234) {
                v = Example.get_visa_status();
                Storage.add(this, v);
                startActivity(new Intent(this, ViewActivity.class));
                return;
            }
        }

        task = new VerifyTask(this, button);
        task.execute(v);
    }

    protected void onStop()
    {
        if (task != null) {
            task.cancel(true);  // experimental
            task.stop();
        }
        super.onStop();
    }

    // Populate the overflow menu.
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.config, menu);
        return true;
    }

    // Called when a menu item is selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.menu_about:
                startActivity(new Intent(this, AboutActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private static final class VerifyTask extends AsyncTask<VisaStatus, Void, VisaStatus>
    {
        private final ConfigActivity    activity;
        private final View              button;
        private PopupWindow             popup;
        private volatile boolean        stopped;

        VerifyTask(ConfigActivity a, View b)
        {
            activity = a;
            button = b;
        }

        protected void onPreExecute()
        {
            button.setClickable(false);
            View v = activity.getLayoutInflater().inflate(R.layout.checking, null);
            popup = new PopupWindow(v, ViewGroup.LayoutParams.WRAP_CONTENT,
                                       ViewGroup.LayoutParams.WRAP_CONTENT);
            popup.showAtLocation(activity.findViewById(R.id.reference), Gravity.CENTER, 0, 0);
        }

        protected VisaStatus doInBackground(VisaStatus... v)
        {
            try {
                v[0].load();
                v[0].error = 0;
            } catch (VisaStatus.VFSException e) {
                v[0].error = R.string.vfsnotfound;      // FIXME
            } catch (Exception e) {
                v[0].error = R.string.netexception;
            }

            return v[0];
        }

        protected void onPostExecute(VisaStatus v)
        {
            if (stopped)
                return;

            stop();

            if (v.error == 0) {
                int n = Storage.add(activity, v);
                activity.startActivity(new Intent(activity,
                            n == 1 ? ViewActivity.class : MultiViewActivity.class));
            } else
                Toast.makeText(activity, v.error, Toast.LENGTH_LONG).show();
        }

        public final void stop()
        {
            popup.dismiss();
            stopped = true;
            activity.task = null;
            button.setClickable(true);
        }
    }

    // Monitor reference to check what kind of second field is required:
    // date of birth or passport expiry date.
    // This doesn't actually filter anything.
    private final class VACFilter implements InputFilter
    {
        private final EditText date_editor;
        private final View button;
        private int last_status = -1;

        public VACFilter(EditText d, View b)
        {
            date_editor = d;
            button = b;
        }

        // This method is called when the buffer is going to replace the range dstart … dend
        // of dest with the new text from the range start … end of source.
        // Return null to accept the original replacement.
        public final CharSequence filter(CharSequence source, int start, int end,
                                         Spanned dest, int dstart, int dend)
        {
            // Get the first 5 characters of the result after replacement,
            // check if they are valid and compare against PLKA/.
            // If the change does not involve the first 5 characters,
            // no action required.
            if (dstart < 5) {
                int status = 0; // 0 to disable, 1 for DoB, 2 for ED
                char[] prefix = new char[5];
                try {
                    int eor = dstart + end - start;
                    for (int i = 0; i < 5; i++) {
                        if (i < dstart)
                            prefix[i] = dest.charAt(i);
                        else if (i < eor)
                            prefix[i] = source.charAt(i - dstart + start);
                        else
                            prefix[i] = dest.charAt(i - eor + dend);
                    }
                    if (prefix[4] == '/') {
                        String vac = new String(prefix, 0, 4);
                        if (vac.equals("PLKA") || vac.equals("SVAC"))
                            status = 2; // Poland or Dubai: passport expiry date
                        else
                            status = 1; // all others: date of birth
                    }
                } catch (RuntimeException e) {
                    // Whether it's index out of bounds or null pointer,
                    // it means the input text is too short and the date field
                    // should stay disabled.
                }

                if (status != last_status) {
                    last_status = status;
                    if (status == 0) {
                        button.setEnabled(false);
                        date_editor.setEnabled(false);
                        date_editor.setHint(null);
                    } else {
                        date_editor.setHint(status == 1 ? R.string.date_of_birth
                                                        : R.string.expiry_date);
                        date_editor.setEnabled(true);
                        button.setEnabled(true);
                    }
                }
            }
            return null;
        }
    }
}
