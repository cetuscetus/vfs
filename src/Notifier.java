/*
 * Copyright 2013 Nikita Schmidt
 *
 * This file is part of VFSCheck.
 *
 * VFSCheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VFSCheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VFSCheck.  If not, see <http://www.gnu.org/licenses/>.
 */

package vfs.check;

//import android.support.v4.app.NotificationCompat;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;

public final class Notifier
{
    private static boolean in_status_bar = true;

    public static void notify(Context c, int num, VisaStatus v)
    {
        if (in_status_bar) {
            String title, text = "";

            if (num > 1) {
                title = c.getResources().getQuantityString(R.plurals.new_updates, num, num);
            } else {
                String status = v.entries.getLast().status;
                int split = status.indexOf(' ', 16) + 1;
                if (split == 0) {
                    title = status;
                } else {
                    title = status.substring(0, split);
                    text = status.substring(split);
                }
            }

            Intent i = new Intent(c, ViewActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pi = PendingIntent.getActivity(c, 0, i, 0);

            Notification notification = new Notification.Builder(c)
                .setSmallIcon(R.drawable.status)
                .setLargeIcon(android.graphics.BitmapFactory.
                    decodeResource(c.getResources(), R.drawable.ic_launcher))
                .setContentTitle(title)
                .setContentText(text)
                .setContentIntent(pi)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS)
                .getNotification();
            ((NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE))
                .notify(0, notification);
        } else {
            Ringtone bell = RingtoneManager.getRingtone(c,
                    android.provider.Settings.System.DEFAULT_NOTIFICATION_URI);
            if (bell != null)
                bell.play();
        }
    }

    public static void stop_status_bar(Context c)
    {
        in_status_bar = false;
        ((NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE)).cancelAll();
    }

    public static void start_status_bar()
    {
        in_status_bar = true;
    }
}
