/*
 * Copyright 2013 Nikita Schmidt
 *
 * This file is part of VFSCheck.
 *
 * VFSCheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VFSCheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VFSCheck.  If not, see <http://www.gnu.org/licenses/>.
 */

package vfs.check;

import android.content.*;
import android.app.*;
import java.util.Date;
import java.util.Locale;
import static android.net.ConnectivityManager.*;

public final class VFSBroadcastReceiver extends BroadcastReceiver
{
    private static final long INEXACT_PERIOD = AlarmManager.INTERVAL_HOUR;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        boolean requested_by_user = false;
        String action = intent.getAction();
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0,
                new Intent("vfs.check.ALARM", null, context, VFSBroadcastReceiver.class), 0);

        if (action.equals(CONNECTIVITY_ACTION)) {
            if (intent.getBooleanExtra(EXTRA_NO_CONNECTIVITY, false))
                // disconnected
                am.setInexactRepeating(am.RTC_WAKEUP,
                        System.currentTimeMillis() + INEXACT_PERIOD,
                        INEXACT_PERIOD,
                        pi);
            else
                // connected
                am.set(am.RTC_WAKEUP, System.currentTimeMillis() + 10 * 1000, pi);
            return;
        }

        if (action.equals("vfs.check.REFRESH"))
            requested_by_user = true;
        else if (!action.equals("vfs.check.ALARM"))
            return;

        VisaStati v = Storage.read(context);

        if (v.isEmpty() || !requested_by_user && v.all_done()) {
            am.cancel(pi);
            return;
        }

        if (!requested_by_user) {
            // avoid checking again too soon after a successful update
            // (to simplify things, we only look at the most recent VisaStatus)
            Date updated = v.lastEntry().getValue().updated;
            if (updated != null) {
                long next = updated.getTime() + INEXACT_PERIOD / 2;
                if ((new Date()).before(new Date(next))) {
                    am.setInexactRepeating(am.RTC_WAKEUP, next, INEXACT_PERIOD, pi);
                    return;
                }
            }
        }

        am.setInexactRepeating(am.RTC_WAKEUP,
                System.currentTimeMillis() + INEXACT_PERIOD,
                INEXACT_PERIOD,
                pi);

        context.startService(new Intent(requested_by_user ? action : null, null,
                    context, UpdateService.class));
    }
}
