/*
 * Copyright 2013 Nikita Schmidt
 *
 * This file is part of VFSCheck.
 *
 * VFSCheck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VFSCheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VFSCheck.  If not, see <http://www.gnu.org/licenses/>.
 */

package vfs.check;

import android.app.Activity;
import android.content.Intent;
import android.webkit.WebView;
import android.webkit.WebSettings;
import android.util.Log;
import java.util.Locale;

public final class InfoActivity extends Activity
{
    private String current_data;
    private WebView webview;
    private String[] assets;

    @Override
    public void onCreate(android.os.Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infopage);

        current_data = "";
        webview = (WebView) findViewById(R.id.infopage);
        WebSettings settings = webview.getSettings();
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);

        try {
            assets = getAssets().list("");
        } catch (java.io.IOException e) {
            assets = new String[0];
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();

        String data = getIntent().getDataString();

        // data is a file name under assets.
        // Let's check if we have a localised version.
        String name;
        String ext;
        int dot = data.lastIndexOf('.');
        if (dot < 0) {
            name = data;
            ext = "";
        } else {
            name = data.substring(0, dot);
            ext = data.substring(dot);
        }
        name += "-" + Locale.getDefault().getLanguage() + ext;
        for (String asset: assets)
            if (asset.equals(name)) {
                data = name;
                break;
            }

        if (!data.equals(current_data))
            try {
                current_data = data;
                setTitle(getIntent().getIntExtra("title", 0));
                webview.loadUrl("file:///android_asset/" + data);
            } catch (Exception e) {
                Log.e("vfs.check", "exception: " + e);
            }
    }
}
