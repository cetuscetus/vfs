package app

import (
    "strings"
    "strconv"
    "regexp"
    "net/http"
    "encoding/json"

    "appengine"
    "appengine/mail"
)

func init() {
    http.HandleFunc("/en/details", details)
    http.HandleFunc("/ru/details", details)
    http.HandleFunc("/", lang)
}

var lang_regexp = regexp.MustCompile(`([a-z]+|\*)(?:-[a-z]+)*(?:;q=([0-9.]+))?`)

// Language negotiation.
func lang(w http.ResponseWriter, r *http.Request) {
    c := appengine.NewContext(r)

    path := r.URL.Path
    if path == "/" {
        path = "/index.html"
    }

    lcode := "en"
    var maxq float64 = 0.

    for _, hdr := range r.Header["Accept-Language"] {
        c.Infof("'" + hdr + "'")
        for _, match := range lang_regexp.FindAllStringSubmatch(strings.ToLower(hdr), 99) {
            q, err := strconv.ParseFloat(match[2], 64)
            if err != nil {
                q = 1.
            }
            c.Infof("- %s '%s' q=%f", match[1], match[2], q)

            if q > maxq && (match[1] == "en" || match[1] == "ru") {
                maxq = q
                lcode = match[1]
            }
        }
    }

    http.Redirect(w, r, "/" + lcode + path, http.StatusFound)
}

func details(w http.ResponseWriter, r *http.Request) {
    if r.Method != "POST" {
        http.Error(w, "Not found", http.StatusNotFound)
        return
    }

    data := struct {
        Country string
        Ref     string
        DoB     string
        Mail    string
        Lang    string
    } {
        Country:    r.FormValue("country"),
        Ref:        r.FormValue("ref"),
        DoB:        r.FormValue("dob"),
        Mail:       r.FormValue("mail"),
        Lang:       r.FormValue("lang"),
    }
    var comments string = r.FormValue("comments")

    if len(data.Country) < 2 || len(data.Country) > 80 ||
            len(data.Ref) < 12 || len(data.Ref) > 32 ||
            len(data.DoB) < 3 || len(data.DoB) > 16 ||
            len(data.Mail) > 255 ||
            len(data.Lang) > 8 ||
            len(comments) > 2000 {
        http.Error(w, "Incorrect form", http.StatusBadRequest)
        return
    }

    c := appengine.NewContext(r)
    text, err := json.MarshalIndent(&data, "", "    ")
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        c.Errorf("%v", err)
        return
    }

    msg := mail.Message {
        Sender: "VFS Check web site <nikita.schmidt@gmail.com>",
        To:     []string { "nikita.schmidt@gmail.com" },
        Subject: "Tracking reference",
        Body:   string(text) + "\n\n" + comments,
    }
    err = mail.Send(c, &msg)
    if err != nil {
        c.Errorf("Couldn't send mail: %v", err)
        http.Error(w, "Internal server error.  Please try again later.",
            http.StatusInternalServerError)
        return
    }

    http.Redirect(w, r, "sent.html", http.StatusFound)
}
