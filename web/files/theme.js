"use strict";

function unspace(id) {
    var v = document.getElementById(id).value.split(/\s+/);
    return v.join("");
}

function show_error(id) {
    var node = document.getElementById(id);
    node.parentElement.style.color = "red";
    node.style.display = "block";
}

function hide_error(name) {
    var node = document.getElementById("bad_" + name);
    if (name == "country") {
        document.getElementById("empty_country").style.display = "none";
    }
    node.parentElement.style.color = "inherit";
    node.style.display = "none";
    if (name == "dob") {
        document.getElementById("dob_remark").style.display = "block";
    } else if (name == "mail") {
        document.getElementById("mail_remark").style.display = "block";
    }
}

function validate() {
    var res = true;

    var country = unspace("country");
    if (country.length == 0) {
        res = false;
        show_error("empty_country");
    } else if (country.length < 2 || country.length > 80) {
        res = false;
        show_error("bad_country");
    }

    var ref = unspace("ref").split("/");
    if (ref.length < 3 || ref.length > 4 ||
            ref[0].length != 4 || ref[1].length != 6 ||
            ref[0].search(/^[a-zA-Z]*$/) < 0 ||
            ref.slice(1).join("").search(/^[0-9]*$/) < 0) {
        res = false;
        show_error("bad_ref");
    }

    var dob = unspace("dob").split("/");
    var bad_dob = false;
    if (dob.length == 3) {
        bad_dob = dob[0].length == 0 || dob[0].length > 2 ||
                  dob[1].length == 0 || dob[0].length > 2 ||
                  (dob[2].length != 2 && dob[2].length != 4) ||
                  dob.join("").search(/^[0-9]*$/) < 0;
    } else if (dob.length = 1) {
        bad_dob = dob[0].length < 3 || dob[0].length > 32 ||
                  dob[0].search(/^[0-9A-Za-z.-_]*$/) < 0;
    } else {
        bad_dob = true;
    }
    if (bad_dob) {
        res = false;
        document.getElementById("dob_remark").style.display = "none";
        show_error("bad_dob");
    }

    if (document.querySelector("#mail:invalid")) {
        res = false;
        document.getElementById("mail_remark").style.display = "none";
        show_error("bad_mail");
    }

    return res;
}
